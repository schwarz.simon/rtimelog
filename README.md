<div align='center'>
    <h1><b>rtimelog</b></h1>
    <p>A terminal time logging utility</p>

![Rust](https://badgen.net/badge/Rust/1.73.0/orange?)
</div>

---

## **ABOUT**

This is a terminal cross-platform rust implementation of the infamous [gtimelog](https://gtimelog.org/).

A simple time tracking application with a simple and human-readable fileformat.

<img src='preview.png' width='400' />

<br />

## **INSTALLATION**

1. Download the appropriate application for your system
2. Place it either in place which is already in your PATH
3. or copy it somerwhere else and add this folder to your PATH

## **BASIC USAGE**

TODO