use clap::{Args, Parser, Subcommand};

#[derive(Debug, Parser)]
#[clap(author, version, about, long_about = None)]
pub struct App {
    #[clap(flatten)]
    pub global_opts: GlobalOpts,

    #[clap(subcommand)]
    pub command: Command,
}


#[derive(Debug, Subcommand)]
pub enum EditKind {
    Log {},
    Tasks {}
}

#[derive(Debug, Subcommand)]
pub enum Command {
    /// Make a new entry
    Entry {
        /// The entry description
        description: String,
        /// The task to add to prefix to description - number form task command
        #[clap(long, short = 't')]
        task: Option<i32>,

        /// Shows available tasks and a menu to choose from to add as prefix to description
        #[clap(long, short = 'c', action)]
        task_chooser: bool,

        /// Correct current time by x minutes
        #[clap(long, short = 'm', allow_hyphen_values(true))]
        minutes_correction: Option<i64>
        
            },
    /// Show entries for today
    Today {
        ///Filter for a string
        filter: Option<String>,

    },
    /// Show entries for current week
    Week {
        ///Filter for a string
        filter: Option<String>,
    },
    /// Show entries for a date
    Date {
        /// The date to show
        date: String,
        ///Filter for a string
        filter: Option<String>,
    },
    /// Show the entries between two dates
    Daterange {
        /// The start date (inclusive)
        date_from: String,
        /// The end date (inclusive)
        date_to: String,
        ///Filter for a string in the entries of the range
        filter: Option<String>,
    },
    /// Show available tasks
    Tasks {

    },
    /// Edit files
    Edit {
        #[clap(subcommand)]
        kind: EditKind,

        /// Specify editor to be used
        #[arg(short, long)]
        editor: Option<String>
    },
}

#[derive(Debug, Args)]
pub struct GlobalOpts {
    //... other global options
}

pub fn parse_args() -> App
{
    App::parse()
}