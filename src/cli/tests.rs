#[cfg(test)]
use super::*;
use std::path::PathBuf;
use std::io::Write;

#[test]
fn test_load_config_default() {
    // Create a temporary home directory
    let temp_dir = tempfile::tempdir().unwrap();

    // Get the path to the config file
    let mut config_file_path: PathBuf = temp_dir.path().to_path_buf();
    config_file_path.push("config.toml");

    // Load the config (which should use defaults)
    let config: CliConfig = match confy::load_path(config_file_path) {
        Ok(config) => config,
        Err(_) => panic!("Failed to load default config"),
    };

    assert_eq!(config.timelog_file, get_timelog_file_path());
    assert_eq!(config.tasks_file, get_tasks_file_path());
    assert_eq!(config.editor, "vim".to_string());
}

#[test]
fn test_load_config_missing() {
    // Create a temporary home directory
    let temp_dir = tempfile::tempdir().unwrap();

    // Get the path to the config file (should be deleted)
    let mut config_file_path: PathBuf = temp_dir.path().to_path_buf();
    config_file_path.push("config.toml");

    // Load the config (which should use defaults)
    let config: CliConfig = match confy::load_path(config_file_path) {
        Ok(config) => config,
        Err(_) => panic!("Failed to load default config"),
    };

    assert_eq!(config.timelog_file, get_timelog_file_path());
    assert_eq!(config.tasks_file, get_tasks_file_path());
    assert_eq!(config.editor, "vim".to_string());
}
