use crate::{parser, cli::arguments::EditKind};
use chrono::{Duration, Local, NaiveDate, NaiveDateTime, Timelike, Weekday};
use std::collections::BTreeMap;
use colored::*;
use home;
use humantime::format_duration;
use serde_derive::{Deserialize, Serialize};
use text_io::read;
use std::process::Command;

mod arguments;
mod cli_duration;
mod tests;

fn get_config_file_path() -> std::path::PathBuf {
    match home::home_dir() {
        Some(path) => {
            path.join(".rtimelog/config.toml")
        }
        None => {
            panic!("Impossible to get your home dir!");
        }
    }
}

fn get_timelog_file_path() -> std::path::PathBuf {
    match home::home_dir() {
        Some(mut path) => {
            path.push(".rtimelog/timelog.txt");
            path
        }
        None => {
            panic!("Impossible to get your home dir!");
        }
    }
}

fn get_tasks_file_path() -> std::path::PathBuf {
    match home::home_dir() {
        Some(mut path) => {
            path.push(".rtimelog/tasks.txt");
            path
        }
        None => {
            panic!("Impossible to get your home dir!");
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(default)]
struct CliConfig {
    timelog_file: std::path::PathBuf,
    tasks_file: std::path::PathBuf,
    editor: String,
}

impl ::std::default::Default for CliConfig {
    fn default() -> Self {
        Self {
            timelog_file: get_timelog_file_path(),
            tasks_file: get_tasks_file_path(),
            editor: "vim".to_string(),
        }
    }
}

fn print_entry(entry: (&NaiveDateTime, &(String, Vec<String>, Duration))) {
    let (date, (description, tags, duration)) = entry;
    print!(
        "{} ({:>6}) - ",
        date.to_string().green(),
        format_duration(duration.to_std().expect("TODO"))
            .to_string()
            .truecolor(235, 235, 52)
    );
    if description.ends_with("**") {
        print!("{}", description.truecolor(115, 115, 115));
    } else {
        print!("{}", description);
    }

    if !tags.is_empty() {
        print!(" --");
        for tag in tags {
            print!(" {}", tag.cyan())
        }
    }
    print!("\n");
}

fn print_tasks(tasks_map: &BTreeMap<i32, String>) {
    println!("Available tasks:");
    for (i, one_entry) in tasks_map {
        print_task_entry(i, one_entry);
    }
}

fn print_task_entry(i: &i32, description: &String) {
    println!("{:0>2}: {}", i.to_string().green(), description);
}

fn print_total_times(worktime: Duration, breaktime: Duration) {
    println!(
        "{:<18} {:>6}",
        "Total work time:",
        cli_duration::format_cli_duration(worktime).truecolor(235, 235, 52)
    );
    println!(
        "{:<18} {:>6}",
        "Total break time:",
        cli_duration::format_cli_duration(breaktime).truecolor(235, 235, 52)
    );
}

fn print_not_logged_time(last_entry_date: &NaiveDateTime, current_date: &NaiveDateTime) {
    let not_logged = current_date
        .with_nanosecond(0)
        .unwrap()
        .with_second(0)
        .unwrap()
        - last_entry_date
            .with_nanosecond(0)
            .unwrap()
            .with_second(0)
            .unwrap();

    println!(
        "{:<18} {:>6}",
        "Not logged yet:",
        cli_duration::format_cli_duration(not_logged).truecolor(235, 235, 52)
    );
}

fn execute_editor(command: String, file: &std::path::Path) {
    let _output = Command::new(command)
        .arg(file)
        .spawn()
        .expect("Failed to execute command")
        .wait()
        .expect("Failed to execute command");
}

pub fn cli() {
    let args = arguments::parse_args();

    let config: CliConfig = match confy::load_path(get_config_file_path()) {
        Ok(config) => {
            //TODO: Make config checks
            config
        },
        Err(e) => {
            panic!("Unable to read or create config file: {}",e);
        }
    };

    //dbg!(config);

    let timelog_parser = parser::TimelogParser::new(  config.timelog_file.as_path());

    match args.command {
        arguments::Command::Entry {
            description,
            task,
            task_chooser,
            minutes_correction,
        } => {
            // Parse the task
            let task = if task_chooser {
                let tasks_parser = parser::TasksParser::new(get_tasks_file_path().as_path());
                let tasks = &tasks_parser.get_tasks();

                print_tasks(tasks);

                print!("Task number: ");
                let task_number: i32 = read!();
                match tasks.get(&task_number) {
                    Some(task) => task.to_string(),
                    None => {
                        //TODO: Maybe a panic is not the right thing here
                        panic!("The given task number does not exist");
                    }
                }
            } else {
                match task {
                    Some(number) => {
                        let tasks_parser = parser::TasksParser::new(get_tasks_file_path().as_path());
                        let tasks = tasks_parser.get_tasks();
                        match tasks.get(&number) {
                            Some(task) => task.to_string(),
                            None => {
                                //TODO: Maybe a panic is not the right thing here
                                panic!("The given task number does not exist");
                            }
                        }
                    }
                    None => {
                        // Do nothing that is ok.
                        String::new()
                    }
                }
            };

            // Parse the minute correction
            let minutes_correction = match minutes_correction {
                Some(minutes) => minutes,
                None => {
                    // Do nothing that is ok.
                    0
                }
            };

            let local: NaiveDateTime =
                Local::now().naive_local() + Duration::minutes(minutes_correction);
            let mut message: String = String::new();
            if !task.is_empty() {
                message = task + ": " + &description
            } else {
                message += &description
            }
            match timelog_parser.append_entry_to_file(local, message)
            {
                Ok(()) => println!("Added entry for today"),
                Err(err) => eprintln!("{}", err)
            }
        }

        arguments::Command::Today { filter } => {
            let local: NaiveDateTime = Local::now().naive_local();
            let from = local
                .with_hour(0)
                .expect("Creation of from date failed (hours)")
                .with_minute(0)
                .expect("Creation of from date failed (minutes)");
            let to = local
                .with_hour(23)
                .expect("Creation of to date failed (hours)")
                .with_minute(59)
                .expect("Creation of to date failed (minutes)");
            let content = timelog_parser.get_range(from, to, filter);

            //TODO: Print this prettier
            println!(
                "Entries for today {}:",
                local.format("%Y-%m-%d").to_string().yellow()
            );
            for one_entry in &content.entries {
                print_entry(one_entry);
            }
            print!("\n");
            print_total_times(content.worktime, content.breaktime);
            let last_date = match content.entries.last_key_value() {
                Some(entry) => entry.0,
                None => &local,
            };
            print_not_logged_time(last_date, &local);
        }

        arguments::Command::Date { date, filter } => {
            let date: NaiveDate = match NaiveDate::parse_from_str(&date, "%Y-%m-%d") {
                Ok(x) => x,
                Err(_) => {
                    panic!("The delivered date could not be parsed. Use format Y-m-d")
                }
            };
            let from = date.and_hms_opt(0, 0, 0).unwrap();
            let to = date.and_hms_opt(23, 59, 59).unwrap();
            let content = timelog_parser.get_range(from, to, filter);

            //TODO: Print this prettier
            println!(
                "Entries for date {}:",
                date.format("%Y-%m-%d").to_string().yellow()
            );
            for one_entry in &content.entries {
                print_entry(one_entry);
            }
            print!("\n");
            print_total_times(content.worktime, content.breaktime);
        }

        arguments::Command::Daterange { date_from, date_to, filter } => {
            let date_from: NaiveDate = match NaiveDate::parse_from_str(&date_from, "%Y-%m-%d") {
                Ok(x) => x,
                Err(_) => {
                    panic!("The delivered date could not be parsed. Use format Y-m-d")
                }
            };

            let date_to: NaiveDate = match NaiveDate::parse_from_str(&date_to, "%Y-%m-%d") {
                Ok(x) => x,
                Err(_) => {
                    panic!("The delivered date could not be parsed. Use format Y-m-d")
                }
            };

            let from = date_from.and_hms_opt(0, 0, 0).unwrap();
            let to = date_to.and_hms_opt(23, 59, 59).unwrap();
            let content = timelog_parser.get_range(from, to, filter);

            //TODO: Print this prettier
            println!(
                "Entries for date range {} to {}:",
                date_from.format("%Y-%m-%d").to_string().yellow(),
                date_to.format("%Y-%m-%d").to_string().yellow()
            );
            for one_entry in &content.entries {
                print_entry(one_entry);
            }
            print!("\n");
            print_total_times(content.worktime, content.breaktime);
        }

        arguments::Command::Week { filter } => {
            let local: NaiveDateTime = Local::now().naive_local();
            let week = local.date().week(Weekday::Mon);

            let from = week.first_day().and_hms_opt(0, 0, 0).unwrap();
            let to = week.last_day().and_hms_opt(23, 59, 59).unwrap();
            let content = timelog_parser.get_range(from, to, filter);
            //TODO: Print this prettier
            println!(
                "Entries for this week {} to {}",
                from.format("%Y-%m-%d").to_string().yellow(),
                to.format("%Y-%m-%d").to_string().yellow()
            );
            for one_entry in &content.entries {
                print_entry(one_entry);
            }
            print!("\n");
            print_total_times(content.worktime, content.breaktime);
        }

        arguments::Command::Tasks {} => {
            let tasks_parser = parser::TasksParser::new(get_tasks_file_path().as_path());
            let tasks = &tasks_parser.get_tasks();

            print_tasks(tasks);
        }

        arguments::Command::Edit { kind, editor } => {
            let editor = match editor {
                Some(editor) => editor,
                None => config.editor
            };
            match kind {
                EditKind::Log {} => {
                    execute_editor(editor, config.timelog_file.as_path());
                }
                EditKind::Tasks {} => {
                    execute_editor(editor, config.tasks_file.as_path());
                }
            }
        }
    }
}
