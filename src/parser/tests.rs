
use std::{path::{PathBuf, Path}, str::FromStr, fs};
use chrono::Datelike;
use tempfile;

use super::*;

#[test]
fn appends_message_to_file() {
    let temp_dir = tempfile::tempdir().unwrap();
    let testfile = temp_dir.path().join("rtimelog.txt");
    let test_string = "Test string".to_string();
    let test_date= NaiveDateTime::parse_from_str("2015-09-05 23:56:04", "%Y-%m-%d %H:%M:%S").unwrap();
    let expected_content = test_date.format("%Y-%m-%d %H:%M").to_string() + ": " + &test_string;

    let ut = TimelogParser::new(testfile.as_path());
    assert!(!ut.append_entry_to_file(test_date, test_string).is_err());


    let content = std::fs::read_to_string(testfile.clone())
    .expect("Should have been able to read the file");

    assert_eq!(content.trim(), expected_content.trim());

    // Clean up
    temp_dir.close().unwrap();
}

#[test]
fn does_not_append_same_date_to_file() {
    let temp_dir = tempfile::tempdir().unwrap();
    let testfile = temp_dir.path().join("testfile.txt");
    let test_string = "Test string".to_string();
    let test_date= NaiveDateTime::parse_from_str("2023-12-24 22:00:04", "%Y-%m-%d %H:%M:%S").unwrap();
    let expected_content = test_date.format("%Y-%m-%d %H:%M").to_string() + ": " + &test_string;

    let ut = TimelogParser::new(testfile.as_path());
    assert!(!ut.append_entry_to_file(test_date, test_string.clone()).is_err());
    assert!(ut.append_entry_to_file(test_date, test_string).is_err());


    let content = std::fs::read_to_string(testfile.clone())
    .expect("Should have been able to read the file");

    assert_eq!(content.trim(), expected_content.trim());
    // Clean up
    temp_dir.close().unwrap();
}

#[test]
fn check_is_date_already_used() {
    let temp_dir = tempfile::tempdir().unwrap();
    let path = temp_dir.path().join("log.txt");

    let parser = TimelogParser::new(&path);

    assert!(parser.check_is_date_already_used(
        NaiveDateTime::from_timestamp(1643723400, 0)
    ) == false);

    assert!(parser.append_entry_to_file(
        NaiveDateTime::from_timestamp(1643723400, 0),
        "Test message".to_string()
    ).is_ok());

    assert!(parser.check_is_date_already_used(
        NaiveDateTime::from_timestamp(1643723400, 0)
    ) == true);

    // Clean up
    temp_dir.close().unwrap();
}

#[test]
fn convert_line() {
    let date_string = "2022-02-01 12:00: Test message -- tag1 tag2 tag3";
    let (date, description, tags, duration) = TimelogParser::convert_line(date_string, None).unwrap();

    assert_eq!(date.year(), 2022);
    assert_eq!(date.month(), 2);
    assert_eq!(date.day(), 1);

    assert_eq!(description, "Test message");
    assert_eq!(tags.len(), 3);
    assert_eq!(tags[0], "tag1".to_string());
    assert_eq!(tags[1], "tag2".to_string());
    assert_eq!(tags[2], "tag3".to_string());

    let duration = duration.num_seconds();
    assert_eq!(duration, 0);
}