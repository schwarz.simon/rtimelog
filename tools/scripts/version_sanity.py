import tomllib
import git

def get_toml_version():
    with open("Cargo.toml", "rb") as f:
        cargo_data = tomllib.load(f)
    cargo_version = cargo_data["package"]["version"]

    return cargo_version

def get_git_version():
    repo = git.Repo(search_parent_directories=True)

    describe_result = repo.git.describe(tags=True)

    if describe_result[0] == 'v':
        tag = describe_result[1:]
    else:
        tag = describe_result

    return tag

def is_release():
    repo = git.Repo(search_parent_directories=True)
    describe_result = repo.git.describe(tags=True)
    print(describe_result)
    if describe_result.find("-") != -1:
        return False
    else:
        return True

if is_release():
    toml_version = get_toml_version()
    git_version = get_git_version()

    if toml_version == git_version:
        print("Versions are equal")
        exit(0)
    else:
        print("Cargo.toml({}) version does not match git tag({})".format(toml_version, git_version))
        exit(-1)
else:
    print("Not a release version not checking")
    exit(0)